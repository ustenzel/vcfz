module Main where

import BasePrelude hiding ( unlines, option )
import Codec.Gully
import Codec.Spray
import Codec.Stream
import Codec.Vcfz
import Options.Applicative
import Streaming
import System.IO

import qualified Data.ByteString                    as B
import qualified Data.ByteString.Char8              as C
import qualified Data.ByteString.Streaming          as S
import qualified Data.ByteString.Streaming.Internal as S
import qualified Data.Vector.Unboxed.Mutable        as U
import qualified Streaming.Prelude                  as Q
import qualified Codec.Compression.Zlib.Internal    as Z

import_opts :: Parser Options
import_opts = Options
    <$> switch ( long "half" <> short '2' <> help "Use half-precision float" )
    <*> switch ( long "unit-ranges" <> short 'u' <> help "Code unit-range floats in a single byte" )
    <*> (flag' Zlib ( long "zlib" <> short 'z' <> help "Compress wioth Zlib" ) <|>
         flag' Lzma ( long "lzma" <> long "xz" <> short 'x' <> help "Compress with lzma (xz)" ) <|>
         pure None)
    <*> optional (option auto ( long "limit" <> short 'n' <> metavar "NUM" <> help "Stop after NUM records" ))
    <*> switch ( long "row-major" <> short 'r' <> help "Use row-major layout" )

view_opts :: Parser ((Handle -> IO ()) -> IO ())
view_opts = (\fp -> withFile fp ReadMode) <$> strArgument (metavar "FILE") <|> pure ($ stdin)

args :: Parser (IO ())
args = hsubparser $ (command "import" (info (main_from      <$> import_opts) (progDesc "Import a VCF file")))
                 <> (command "view"   (info (pure main_to    <**> view_opts) (progDesc "Export a VCF file")))
                 <> (command "stats"  (info (pure main_stats <**> view_opts) (progDesc "Collect statistics")))

main :: IO ()
main = join . execParser $ info
            (args <**> helper)
            (fullDesc <> progDesc "Experimental VCF compression" <> header "whatever")

main_from :: Options -> IO ()
main_from opts = S.hPut stdout $ do
    S.chunk $ makeHeader opts

    let hdr = Q.span (C.isPrefixOf "#" . snd) . lines' 1 [] $ S.hGetContents stdin
    body <- gzip $ Q.foldrT ((>>) . S.chunk . snd) hdr

    gs <- liftIO . newGully $ case use_compression opts of None -> noop
                                                           Zlib -> compress_zlib
                                                           Lzma -> compress_lzma
    Q.foldM_ (parseVcf opts gs) (pure $ Loc "" 0) (const $ pure ())
             $ hoist lift $ maybe id Q.take (use_limit opts) $ body
    cleanGully gs

main_to :: Handle -> IO ()
main_to hdl = S.hPut stdout $ do
    Options{..} :> more <- Q.mapOf fromHeader <$> lift (S.toStrict (S.splitAt 4 (S.hGetContents hdl)))
    liftIO $ hPrint stderr Options{..}
    body <- gunzip1 more                 -- gunzip and yield header text
    sp <- newSpray $ case use_compression of None -> noop
                                             Zlib -> decompress_zlib
                                             Lzma -> decompress_lzma
    S.toStreamingByteString $ S.concatBuilders $ printVcf Options{..} sp body


gunzip1 :: MonadIO m => S.ByteString m r -> S.ByteString m (S.ByteString m r)
gunzip1 = flip go $ Z.decompressIO Z.gzipOrZlibFormat Z.defaultDecompressParams
  where
    go (S.Chunk c inp) (Z.DecompressInputRequired next) | B.null c  = go inp (Z.DecompressInputRequired next)
                                                        | otherwise = liftIO (next c) >>= go inp
    go (S.Go m)        (Z.DecompressInputRequired next) = lift m >>= flip go (Z.DecompressInputRequired next)
    go (S.Empty r)     (Z.DecompressInputRequired next) = liftIO (next B.empty) >>= go (pure r)
    go  inp (Z.DecompressOutputAvailable outchunk next) = S.chunk outchunk >> liftIO next >>= go inp
    go  inp (Z.DecompressStreamEnd             inchunk) = pure (S.chunk inchunk >> inp)
    go _inp (Z.DecompressStreamError              derr) = liftIO $ throwIO derr

gzip :: MonadIO m => S.ByteString m r -> S.ByteString m r
gzip = go $ Z.compressIO Z.gzipFormat Z.defaultCompressParams
  where
    go (Z.CompressInputRequired next) (S.Chunk ck inp) | B.null ck = go (Z.CompressInputRequired next) inp
                                                       | otherwise = liftIO (next ck) >>= flip go inp
    go (Z.CompressInputRequired next)  (S.Go    m) = lift m >>= go (Z.CompressInputRequired next)
    go (Z.CompressInputRequired next)  (S.Empty r) = liftIO (next B.empty) >>= flip go (pure r)
    go (Z.CompressOutputAvailable ochunk next) inp = S.chunk ochunk >> liftIO next >>= flip go inp
    go  Z.CompressStreamEnd                    inp = lift (S.effects inp)

lines' :: Monad m => Int -> [B.ByteString] -> S.ByteString m r -> Stream (Of (Int, B.ByteString)) m r
lines' !num acc (S.Empty    r)   = r <$ unless (all B.null acc) (Q.yield (num, B.concat (reverse ("\n":acc))))
lines' !num acc (S.Go       m)   = lift m >>= lines' num acc
lines' !num acc (S.Chunk c cs)
    | Just i <- B.elemIndex 10 c = Q.cons (num, if null acc then B.take (i+1) c
                                                            else B.concat (reverse (B.take (i+1) c : acc)))
                                          (lines' num [] (S.Chunk (B.drop (i+1) c) cs))
    | otherwise                  = lines' num (c:acc) cs

main_stats :: Handle -> IO ()
main_stats hdl = do
    v <- U.replicate (1 + fromEnum (maxBound :: Field)) (0::Int)

    Options{..} :> more <- Q.mapOf fromHeader <$> S.toStrict (S.splitAt 4 (S.hGetContents hdl))
    liftIO $ hPrint stderr Options{..}
    go v =<< S.effects (gunzip1 more)                 -- gunzip and discard header text, process rest

    sequence_ [ U.read v (fromEnum i) >>= \l -> hPrintf stdout "%10s %10db (%4dMb)\n" (show i) l (l `shiftR` 20)
              | i <- [minBound .. maxBound :: Field] ]
  where
    go v s = getChunk s >>= \case
        (Left      _,_ ) -> return ()
        (Right (i,c),s') -> U.modify v (+ B.length c) i >> go v s'


