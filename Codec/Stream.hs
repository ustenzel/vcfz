module Codec.Stream where

import BasePrelude
import qualified Data.ByteString                 as B
import qualified Codec.Compression.Lzma          as X
import qualified Codec.Compression.Zlib.Internal as Z

data CodedStream
    = InputRequired   (B.ByteString -> CodedStream)
    | OutputAvailable !B.ByteString CodedStream
    | CodecGo         (IO CodedStream)
    | CodecError      String
    | StreamEnd

-- | The no-op codec.  Compression and decompression are identical and
-- do nothing.
noop :: CodedStream
noop = InputRequired go
  where
    go chunk | B.null chunk = StreamEnd
             | otherwise    = OutputAvailable chunk (InputRequired go)

-- | Compresses to ZLib format.
compress_zlib :: CodedStream
compress_zlib = go (Z.compressIO Z.zlibFormat Z.defaultCompressParams)
  where
    go (Z.CompressInputRequired         k) = InputRequired (\chunk -> CodecGo (go <$> k chunk))
    go (Z.CompressOutputAvailable chunk k) = OutputAvailable chunk (CodecGo (go <$> k))
    go  Z.CompressStreamEnd                = StreamEnd

-- | Compresses to ZLib format.
decompress_zlib :: CodedStream
decompress_zlib = go (Z.decompressIO Z.zlibFormat Z.defaultDecompressParams)
  where
    go (Z.DecompressInputRequired         k) = InputRequired (\chunk -> CodecGo (go <$> k chunk))
    go (Z.DecompressOutputAvailable chunk k) = OutputAvailable chunk (CodecGo (go <$> k))
    go (Z.DecompressStreamEnd _)             = StreamEnd
    go (Z.DecompressStreamError e)           = CodecError (show e)


-- | Compresses to XZ format.
compress_lzma :: CodedStream
compress_lzma = CodecGo (go <$> X.compressIO X.defaultCompressParams)
  where
    go (X.CompressInputRequired       _ k) = InputRequired (\chunk -> CodecGo (go <$> k chunk))
    go (X.CompressOutputAvailable chunk k) = OutputAvailable chunk (CodecGo (go <$> k))
    go  X.CompressStreamEnd                = StreamEnd

-- | Compresses to XZ format.
decompress_lzma :: CodedStream
decompress_lzma = CodecGo (go <$> X.decompressIO X.defaultDecompressParams)
  where
    go (X.DecompressInputRequired         k) = InputRequired (\chunk -> CodecGo (go <$> k chunk))
    go (X.DecompressOutputAvailable chunk k) = OutputAvailable chunk (CodecGo (go <$> k))
    go (X.DecompressStreamEnd _)             = StreamEnd
    go (X.DecompressStreamError e)           = CodecError (show e)

