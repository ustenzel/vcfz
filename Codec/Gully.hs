module Codec.Gully
    ( Gully
    , newGully
    , pour
    , cleanGully
    ) where

import BasePrelude
import Codec.Stream
import Foreign.C.Types                      ( CChar )
import Foreign.Marshal.Alloc                ( mallocBytes )

import qualified Data.ByteString              as B
import qualified Data.ByteString.Streaming    as S
import qualified Data.ByteString.Unsafe       as B
import qualified Data.Vector.Mutable          as V
import qualified Data.Vector.Storable.Mutable as W

-- | A typed drain that feeds into a larger sewer.  Implemented as a binary buffer, which is periodically
-- emptied and run through a compressor.  The type parameter is the type
-- of field indices.
data Gully t = Gully { buffer :: W.IOVector (Ptr CChar)
                     , next   :: W.IOVector (Ptr CChar)
                     , end    :: W.IOVector (Ptr CChar)
                     , strm   :: V.IOVector CodedStream }

-- | Input is a vector of desired buffer sizes.
newGully :: (Enum t, Bounded t) => CodedStream -> IO (Gully t)
newGully = newGully' minBound maxBound

newGully' :: Enum t => t -> t -> CodedStream -> IO (Gully t)
newGully' x y comp = do
    let l = fromEnum y - fromEnum x + 1
    buffer <- W.new l
    next   <- W.new l
    end    <- W.new l
    strm   <- V.new l

    forM_ [0..l-1] $ \i -> do
        b <- mallocBytes size
        W.write buffer i b
        W.write next   i b
        W.write end    i $ b `plusPtr` size
        V.write strm   i comp

    return Gully{..}
  where
    size = 32760


-- | Pours something down a gully.  A stream may may not come out in response.
pour :: (MonadIO m, Storable a, Enum t, Show t) => t -> a -> Gully t -> S.ByteString m ()
pour t a Gully{..} = do
    let i = fromEnum t
    e <- liftIO $ W.read end i
    n <- liftIO $ W.read next i

    if e `minusPtr` n < sizeOf a then do
        b0 <- liftIO $ W.read buffer i
        s0 <- liftIO $ V.read strm i
        let used = n `minusPtr` b0
        let sz   = e `minusPtr` b0

        b1 <- liftIO $ mallocBytes sz
        liftIO $ poke (castPtr b1) a

        liftIO $ W.write buffer i b1
        liftIO . W.write next   i $ b1 `plusPtr` sizeOf a
        liftIO . W.write end    i $ b1 `plusPtr` sz
        liftIO . V.write strm i
            =<< drain t s0
            =<< liftIO (B.unsafePackMallocCStringLen (b0, used))
      else do
        liftIO $ poke (castPtr n) a
        liftIO $ W.write next i $ plusPtr n (sizeOf a)
  where
    drain _ (InputRequired      k) c = return (k c)
    drain i (OutputAvailable ck k) c = yield_chunk i ck >> drain i k c
    drain i (CodecGo k)            c = liftIO k >>= \k' -> drain i k' c
    drain _ (CodecError e)        _c = error e
    drain _  StreamEnd            _c = error "CompressStream ended unexpectedly"


cleanGully :: (MonadIO m, Bounded t, Enum t, Show t) => Gully t -> S.ByteString m ()
cleanGully = cleanGully' minBound maxBound

cleanGully' :: (MonadIO m, Enum t, Show t) => t -> t -> Gully t -> S.ByteString m ()
cleanGully' x y Gully{..} =
    forM_ [x..y] $ \t -> do
        let i = fromEnum t
        s <- liftIO $ V.read strm i
        b <- liftIO $ W.read buffer i
        n <- liftIO $ W.read next i
        let used = n `minusPtr` b
        drain t s =<< liftIO (B.unsafePackMallocCStringLen (b,used))
  where
    drain i (InputRequired      k) c = drain i (k c) mempty
    drain i (OutputAvailable ck k) c = yield_chunk i ck >> drain i k c
    drain i (CodecGo k)            c = liftIO k >>= \k' -> drain i k' c
    drain _ (CodecError e)        _c = error e
    drain _  StreamEnd            _c = pure ()


-- | Encodes the tag value and the size of the chunk.  Tags can be up to 255, size up to 2^16-1.
yield_chunk :: (MonadIO m, Enum t, Show t) => t -> B.ByteString -> S.ByteString m ()
yield_chunk t c
    | fromEnum t < 0 || fromEnum t > 0xff = error "tag value out of range"
    | l < 0 || l > 0xffff                 = error "chunk length out of range"
    | otherwise                           = S.chunk h >> S.chunk c
  where
    l = B.length c
    h = B.pack [ fromIntegral (fromEnum t), fromIntegral l, fromIntegral (l `shiftR` 8) ]

