{-# LANGUAGE MagicHash, UnliftedFFITypes #-}
module Codec.Vcfz
    ( parseVcf
    , Options(..)
    , Compression(..)
    , fromHeader
    , makeHeader
    , Field(..)
    , printVcf
    , Loc(..)
    ) where

import BasePrelude                           hiding ( readFloat )
import Control.Monad.Trans.RWS.Strict
import Data.ByteString.Builder
import Foreign.C.Types
import Foreign.Marshal.Alloc
import GHC.Prim
import Numeric.Half
import Streaming
import System.IO                                    ( stderr )

import qualified Data.ByteString                       as B
import qualified Data.ByteString.Builder.Prim          as BP
import qualified Data.ByteString.Builder.Prim.Internal as BP
import qualified Data.ByteString.Char8                 as C
import qualified Data.ByteString.Streaming             as S
import qualified Data.ByteString.Unsafe                as B
import qualified Codec.Gully                           as G
import qualified Codec.Spray                           as T

-- Overview:
--
-- * Parse VCF and directly transcode it.  (Representing VCF in a
--   sufficiently generic data type is a fool's errand.)  We make this
--   very rigid to open up opportunities for efficient encodings.
--
-- * Shred the data into one stream per field.  The first is the record
--   type, which determines which other fields are used in the same
--   record.  Each field ends up being a homogenous stream of rather
--   similar values.
--
--   * A single record always produces at least one tag, but zero or
--     more entries per other field.
--
--   * Special records to jump to another chromosome or within the
--     current one are interleaved into the stream of tag fields.  This
--     is done because in streams on their own, they would rarely output
--     blocks and thereby cause excessive buffering.
--
-- * Encode each field-stream in binary and compress it.  Compression
--   could be gzip or lzma, or it could be a field-specific
--   transformation followed by a generic compressor.
--
--   * Floats can now optinally be reduced to 'Half' to conserve space.
--     The cost is reduced precision, which isn't needed anyway.
--
-- * Interleave blocks from multiple compressed streams into one stream
--   of tagged blocks.
--
--   * The VCF header is compressed (Zlib format) and prepended.  It's
--     not worth pouring more effort into it.

-- Notes:
-- * ID, FILTER is not stored, because it isn't used in our data.  (Dumb idea anyway.)
-- * QUAL needs an data type appropriate for its range and precision.  'Half' may be good.
-- * QUAL for Invar and for SNP (may) have very different distributions
-- * Integers use a simple variable width encoding.  Most integers are small, after all.
-- * some values are Floats in the range [0..2], and can optionally be encoded as Word8s, with the
--   understanding that @x@ really means @x/254@ (so 255 can be the \"missing\" value)
-- * other values are in the range [-1..1], and similarly, @x@ could mean @x/127 - 1@.

data Options = Options { use_half        :: Bool
                       , use_unit_ranges :: Bool
                       , use_compression :: Compression
                       , use_limit       :: Maybe Int
                       , use_row_major   :: Bool }
  deriving Show

data Compression = None | Zlib | Lzma deriving (Show, Enum)

-- Header is ASCII "ZCF", the next byte contains four flags (only bit 0
-- is used) and a version number in the upper four bits (currently 0).
makeHeader :: Options -> B.ByteString
makeHeader Options{..} = B.pack [90,67,70,flags]
  where
    flags = fromIntegral  $  fromEnum use_half        `shiftL` 0
                         .|. fromEnum use_unit_ranges `shiftL` 1
                         .|. fromEnum use_compression `shiftL` 2
                         .|. fromEnum use_row_major   `shiftL` 7

fromHeader :: B.ByteString -> Options
fromHeader s = case B.unpack s of
    [90,67,70,flags] -> Options { use_half        = testBit flags 0
                                , use_unit_ranges = testBit flags 1
                                , use_compression = toEnum $ fromIntegral (shiftR flags 2 .&. 0x1f)
                                , use_limit       = Nothing
                                , use_row_major   = testBit flags 7 }
    _                -> error $ "invalid header " ++ show s

data Type
    = SetChrom          -- start a new chromosome, don't output anything
    | SetPos            -- jump to position, don't output anything
    | Invar             -- no variant at _next_ position
    | SnpHom            -- hom/alt snp at next position
    | SnpHomMulti       -- hom/alt snp at next position
    | SnpHet            -- het snp at next position
    | SnpHetMulti       -- het snp at next position
    | IndelAlt          -- indel/alt at __this__ position
    | IndelAltMulti     -- indel/alt at __this__ position
    | IndelHet          -- het indel at __this__ position
    | IndelHetMulti     -- het indel at __this__ position
    | IndelRef          -- indel, but no var at __this__ position
  deriving Enum

instance Storable Type where
    sizeOf _ = 1
    alignment _ = 1
    peek p = toEnum . fromIntegral <$> peek (castPtr p :: Ptr Word8)
    poke p = poke (castPtr p :: Ptr Word8) . fromIntegral . fromEnum

data Field = Typecode
           | Qualities
           | Snp_base
           | Indel_seq
           | Dp
           | Dp4
           | Rpb
           | Bqb
           | Mqb
           | Mqsb
           | Mq0f
           | Af1
           | Ac1
           | Mq
           | Fq
           | Vdb
           | Sgb
           | Idv
           | Imf
           | Pv
           | Pl
  deriving ( Show, Enum, Bounded )


emit_pos :: MonadIO m => Int32 -> m ()
emit_pos p = do
    when (p .&. 0xfff == 0) $ liftIO $ hPrintf stderr "\027[K%d\r" p

emit_chrom :: MonadIO m => B.ByteString -> m ()
emit_chrom c = liftIO $ hPutBuilder stderr $ "\027[K" <> byteString c <> "\n"

tab, semi, comma :: Monad m => P m ()
tab   = lit "\t"
semi  = lit ";"
comma = lit ","

lit :: Monad m => B.ByteString -> P m ()
lit t = do
    s <- get
    if t `C.isPrefixOf` s then
        put $ C.drop (C.length t) s
      else do
        perror $ "expected " ++ show t ++ ", but got " ++ show s

word :: Monad m => P m B.ByteString
word = state $ C.span (/= '\t')

field :: Monad m => P m B.ByteString
field = state (C.span (/= ':')) <* modify (C.drop 1)

int :: Monad m => P m Int32
int = do
    s <- get
    case C.readInt s of
        Just (x,s') -> do put s' ; pure (fromIntegral x)
        Nothing     -> perror $ "expected int, but got " ++ show s

float :: Monad m => P m Float
float = do
    s <- get
    case readFloat s of
        Just (x,s') -> do put s' ; pure x
        Nothing     -> perror $ "expected float, but got " ++ show s

newtype Nucleotide = N { unN :: Word8 } deriving ( Eq, Ord, Ix, Storable )

nucs :: Monad m => P m [Nucleotide]
nucs = B.foldr (\x -> case x .|. 32 of 97 -> (N 0:); 99 -> (N 1:); 103 -> (N 2:); _ -> (N 3:)) []
       <$> state (B.span (\c -> c /= 9 && c /= 44))

many_nucs :: Monad m => P m [[Nucleotide]]
many_nucs = do
    s <- get
    if C.null s || C.head s == '.' then do
        put (C.drop 1 s)
        return []
      else go
  where
    go = liftM2 (:) nucs (maybe [] id <$> iflit "," go)

showNuc :: Nucleotide -> Builder
showNuc (N 0) = char7 'A'
showNuc (N 1) = char7 'C'
showNuc (N 2) = char7 'G'
showNuc   _   = char7 'T'

data GT = Ref2 | Het | Alt2 deriving Show

-- location in genome
data Loc = Loc !B.ByteString !Int32

parseVcf :: MonadIO m => Options -> G.Gully Field -> Loc -> (Int, B.ByteString) -> S.ByteString m Loc
parseVcf Options{..} gs (Loc chrom0 pos0) =
  runShitParser $
  guard_eol (Loc chrom0 pos0) $ do
    chrom <- word              -- CHROM
    tab
    when (chrom /= chrom0) $ do
        pour Typecode SetChrom
        pour_w8 Typecode . fromIntegral $ B.length chrom
        mapM_ (pour_w8 Typecode) (B.unpack chrom)
        emit_chrom chrom

    pos <- int                 -- POS
    emit_pos pos
    when (pos /= pos0) $ do
        pour Typecode SetPos
        pour_w32 Typecode pos

    lit "\t.\t"                 -- ID
    ref <- nucs                 -- REF
    tab
    alt <- many_nucs            -- ALT
    tab
    pour_float Qualities =<< float    -- QUAL
    tab
    lit "."                     -- FILTER
    tab

    isindel <- iflit "INDEL" semi
    iflit "IDV="    (int <* semi) >>= pour_mw32 Idv
    iflit "IMF="  (float <* semi) >>= pour_mfloat Imf
    dp <- lit "DP=" *> int <* semi
    pour_w32 Dp dp
    iflit "VDB="  (float <* semi) >>= pour_mfloat_unit Vdb
    iflit "SGB="  (float <* semi) >>= pour_mfloat_sunit Sgb
    iflit "RPB="  (float <* semi) >>= pour_mfloat_unit Rpb
    iflit "MQB="  (float <* semi) >>= pour_mfloat_unit Mqb
    iflit "MQSB=" (float <* semi) >>= pour_mfloat_unit Mqsb
    iflit "BQB="  (float <* semi) >>= pour_mfloat_unit Bqb
    iflit "MQ0F=" (float <* semi) >>= pour_mfloat Mq0f
    iflit "AF1="  (float <* semi) >>= pour_mfloat_unit Af1
    lit   "AC1="  *> int <* semi  >>= pour_w32 Ac1
    lit   "DP4="
    dp4a <- int <* comma
    dp4b <- int <* comma
    dp4c <- int <* comma
    dp4d <- int <* semi
    lit "MQ=" *> (int <* semi) >>= pour_w32 Mq
    lit "FQ=" *> float >>= pour_float Fq
    iflit ";PV4=" ((,,,) <$> float <* comma <*> float <* comma <*> float <* comma <*> float) >>= maybe
        (pour_float Pv (0/0)) (\(a,b,c,d) -> pour_float Pv a >> pour_float Pv b >> pour_float Pv c >> pour_float Pv d)
    lit "\tGT:PL\t"

    gt <- field >>= \case
            "0/0" -> do pour_i32 Dp4 $ dp4a - dp `div` 2
                        pour_i32 Dp4 $ dp4b - dp `div` 2
                        pour_i32 Dp4 $ dp4c
                        pour_i32 Dp4 $ dp4d
                        pure Ref2

            "0/1" -> do pour_i32 Dp4 $ dp4a - dp `div` 4
                        pour_i32 Dp4 $ dp4b - dp `div` 4
                        pour_i32 Dp4 $ dp4c - dp `div` 4
                        pour_i32 Dp4 $ dp4d - dp `div` 4
                        pure Het

            "1/1" -> do pour_i32 Dp4 $ dp4a
                        pour_i32 Dp4 $ dp4b
                        pour_i32 Dp4 $ dp4c - dp `div` 2
                        pour_i32 Dp4 $ dp4d - dp `div` 2
                        pure Alt2

            x     -> perror $ "GT = " ++ show x

    let npl = (1 + length alt) * (2 + length alt) `div` 2
    replicateM_ (npl-1) $ int <* comma >>= pour_w32 Pl
    int          >>= pour_w32 Pl

    case isindel :> map length alt :> gt of
        Nothing :> [] :> Ref2 -> do
            pour Typecode Invar
            pour Snp_base (head ref)

        Nothing :> [1] :> Het -> do
            pour Typecode SnpHet
            pour Snp_base (head ref)
            pour Snp_base (head (head alt))

        Nothing :> [1] :> Alt2 -> do
            pour Typecode SnpHom
            pour Snp_base (head ref)
            pour Snp_base (head (head alt))

        Just () :> [] :> Ref2 -> do
            pour Typecode IndelRef
            pour_seq Indel_seq ref

        Just () :> [_] :> Het -> do
            pour Typecode IndelHet
            pour_seq Indel_seq ref
            pour_seq Indel_seq (head alt)

        Just () :> [_] :> Alt2 -> do
            pour Typecode IndelAlt
            pour_seq Indel_seq ref
            pour_seq Indel_seq (head alt)

        Nothing :> vs :> Het | all (== 1) vs -> do    -- SNP case
            pour Typecode SnpHetMulti
            pour_w8 Typecode $ fromIntegral (length alt)
            pour Snp_base (head ref)
            mapM_ (pour Snp_base . head) alt

        Nothing :> vs :> Alt2 | all (== 1) vs -> do    -- SNP case
            pour Typecode SnpHomMulti
            pour_w8 Typecode $ fromIntegral (length alt)
            pour Snp_base (head ref)
            mapM_ (pour Snp_base . head) alt

        -- Just () :> _ :> Ref2 -> doesn't happen?

        Just () :> _ :> Het -> do
            pour Typecode IndelHetMulti
            pour_w8 Typecode $ fromIntegral (length alt)
            pour_seq Indel_seq ref
            mapM_ (pour_seq Indel_seq) alt

        Just () :> _ :> Alt2 -> do
            pour Typecode IndelAltMulti
            pour_w8 Typecode $ fromIntegral (length alt)
            pour_seq Indel_seq ref
            mapM_ (pour_seq Indel_seq) alt

        Nothing   :> y :> z -> perror $ printf "Unhandled SNP, %d vars (%s), GT %s" (length y) (show y) (show z)
        Just () :> y :> z -> perror $ printf "Unhandled INDEL, %d vars (%s), GT %s" (length y) (show y) (show z)

    case isindel of Nothing   -> pure $ Loc chrom (succ pos)
                    Just () -> pure $ Loc chrom pos
  where
    pour fld a = lift $ G.pour (if use_row_major then toEnum 0 else fld) a gs
    pour_seq f s = pour f (fromIntegral (length s) :: Word8) >> mapM_ (pour f . unN) s
    pour_w8  f a = pour f (a :: Word8)

    pour_mfloat f  Nothing = pour_float f (0/0)
    pour_mfloat f (Just a) = pour_float f a

    pour_mfloat_unit f ma | not use_unit_ranges = pour_mfloat f ma
    pour_mfloat_unit f Nothing = pour_w8 f 255
    pour_mfloat_unit f (Just a) | a < 0 || a > 2 = error $ "'unit' can't do that: " ++ show a
                                | otherwise      = pour_w8 f (round $ a * 127)

    pour_mfloat_sunit f ma | not use_unit_ranges = pour_mfloat f ma
    pour_mfloat_sunit f Nothing = pour_w8 f 255
    pour_mfloat_sunit f (Just a) | a < -1 || a > 1 = error $ "'sunit' can't do that: " ++ show a
                                 | otherwise      = pour_w8 f (round $ (a+1) * 127)

    pour_w32 f a | a < 0            =    error "Didn't expect that!"
                 | a < 0x80         =    pour_w8 f $ fromIntegral (a :: Int32)
                 | a < 0x4000       = do pour_w8 f $ fromIntegral (a `shiftR` 8) .|. 0x80
                                         pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | a < 0x200000     = do pour_w8 f $ fromIntegral (a `shiftR` 16) .|. 0xC0
                                         pour_w8 f $ fromIntegral (a `shiftR` 8)
                                         pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | a < 0x10000000   = do pour_w8 f $ fromIntegral (a `shiftR` 24) .|. 0xE0
                                         pour_w8 f $ fromIntegral (a `shiftR` 16)
                                         pour_w8 f $ fromIntegral (a `shiftR` 8)
                                         pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | otherwise        = do pour_w8 f 0xF0
                                         pour_w8 f $ fromIntegral (a `shiftR` 24)
                                         pour_w8 f $ fromIntegral (a `shiftR` 16)
                                         pour_w8 f $ fromIntegral (a `shiftR` 8)
                                         pour_w8 f $ fromIntegral (a `shiftR` 0)

    pour_mw32 f  Nothing = pour_w8  f (maxBound :: Word8)
    pour_mw32 f (Just a) = pour_w32 f (a :: Int32)

    pour_i32 f a | -0x40 < a && a < 0x40            =     pour_w8 f $ fromIntegral (a :: Int32) .&. 0x7f
                 | -0x2000 < a && a < 0x2000        =  do pour_w8 f $ fromIntegral (a `shiftR` 8) .&. 0x3f .|. 0x80
                                                          pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | -0x100000 < a && a < 0x100000    =  do pour_w8 f $ fromIntegral (a `shiftR` 16) .&. 0x1f .|. 0xC0
                                                          pour_w8 f $ fromIntegral (a `shiftR` 8)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | -0x8000000 < a && a < 0x8000000  =  do pour_w8 f $ fromIntegral (a `shiftR` 24) .&. 0xf .|. 0xE0
                                                          pour_w8 f $ fromIntegral (a `shiftR` 16)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 8)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 0)
                 | otherwise                        =  do pour_w8 f $ fromIntegral (a `shiftR` 24) .&. 0x7 .|. 0xF0
                                                          pour_w8 f $ fromIntegral (a `shiftR` 24)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 16)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 8)
                                                          pour_w8 f $ fromIntegral (a `shiftR` 0)

    pour_float f a | use_half  = pour f (toHalf a)
                   | otherwise = pour f (a :: Float)
{-# INLINABLE parseVcf #-}

guard_eol :: Monad m => a -> P m a -> P m a
guard_eol r p = do
    s <- get
    if B.null s || B.head s == 10 || B.head s == 13
      then pure r
      else p

iflit :: Monad m => B.ByteString -> P m a -> P m (Maybe a)
iflit key m = do
    s <- get
    if key `C.isPrefixOf` s then do
        put (C.drop (C.length key) s)
        fmap Just m
      else
        pure Nothing

type P m a = RWST (Int, B.ByteString) () B.ByteString m a

runShitParser :: Monad m => P m a -> (Int, B.ByteString) -> m a
runShitParser p (l,s) = do
    (a,s',()) <- runRWST p (l,s) s
    case s' of
        ""     -> pure a
        "\n"   -> pure a
        "\r\n" -> pure a
        _      -> error $ printf "Incomplete parse at line %d, %s remains of %s.\n" l (show s') (show s)

perror :: Monad m => String -> P m a
perror m = do
    (l,s) <- ask
    t <- get
    error $ printf "%s while parsing line %d: %s remaining of %s" m l (show t) (show s)


-- Blows up if s isn't NUL-terminated and completely parses as a number.
-- Can't happen, because evenrything we parse has a terminating newline.
readFloat :: B.ByteString -> Maybe (Float, B.ByteString)
readFloat s | l == 0     =  Nothing
            | otherwise  =  Just (x, C.drop l s)
  where
    (x,l) = unsafeDupablePerformIO $
            B.unsafeUseAsCString s $ \p ->
            alloca $ \pe ->
            liftM2 (,) (strtof p pe) (fmap (`minusPtr` p) (peek pe))


printVcf :: Options -> T.Spray Field -> S.ByteString IO () -> Stream (Of Builder) IO ()
printVcf o s = go (Loc "" 0)
  where
    go loc t = do
        (loc',t',b) <- lift $ runRWST (do ee <- T.sprayEnded Typecode
                                          if ee then pure Nothing
                                                else Just <$> printVcf1 o loc) s t
        maybe (pure ()) (\l -> wrap $ b :> go l t') loc'


printVcf1 :: Options -> Loc -> RWST (T.Spray Field) Builder (S.ByteString IO ()) IO Loc
printVcf1 Options{..} (Loc chrom0 pos0) = do
    (chrom, pos, tp) <- let go c p = drip Typecode >>= \case
                                            SetChrom -> do l <- drip_w8 Typecode
                                                           cs <- replicateM (fromIntegral l) (drip_w8 Typecode)
                                                           go (B.pack cs) p
                                            SetPos -> drip_w32 Typecode >>= go c
                                            t -> pure (c,p,t)
                        in go chrom0 pos0

    when (chrom /= chrom0) $ emit_chrom chrom
    emit_pos pos
    tell $ byteString chrom <> "\t" <> int32Dec pos <> "\t.\t"

    (gt, isindel, nalt) <-
        case tp of
            Invar -> do drip Snp_base >>= tell . showNuc
                        tell "\t."
                        pure (Ref2, False, 0)

            SnpHet -> do drip Snp_base >>= tell . showNuc
                         tell "\t"
                         drip Snp_base >>= tell . showNuc
                         pure (Het, False, 1)

            SnpHetMulti -> do na <- fromIntegral <$> drip_w8 Typecode
                              drip Snp_base >>= tell . showNuc
                              tell "\t"
                              replicateM_ (na-1) $ drip Snp_base >>= tell . showNuc . N >> tell ","
                              drip Snp_base >>= tell . showNuc
                              pure (Het, False, na)

            SnpHom -> do drip Snp_base >>= tell . showNuc
                         tell "\t"
                         drip Snp_base >>= tell . showNuc
                         pure (Alt2, False, 1)

            SnpHomMulti -> do na <- fromIntegral <$> drip_w8 Typecode
                              drip Snp_base >>= tell . showNuc
                              tell "\t"
                              replicateM_ (na-1) $ drip Snp_base >>= tell . showNuc . N >> tell ","
                              drip Snp_base >>= tell . showNuc
                              pure (Alt2, False, na)

            IndelRef -> do drip_seq Indel_seq >>= tell . foldMap showNuc
                           tell "\t."
                           pure (Ref2, True, 0)

            IndelHet -> do drip_seq Indel_seq >>= tell . foldMap showNuc
                           tell "\t"
                           drip_seq Indel_seq >>= tell . foldMap showNuc
                           pure (Het, True, 1)

            IndelHetMulti -> do na <- fromIntegral <$> drip_w8 Typecode
                                drip_seq Indel_seq >>= tell . foldMap showNuc
                                tell "\t"
                                replicateM_ (na-1) $
                                    drip_seq Indel_seq >>= tell . foldMap showNuc >> tell ","
                                drip_seq Indel_seq >>= tell . foldMap showNuc
                                pure (Het, True, na)

            IndelAlt -> do drip_seq Indel_seq >>= tell . foldMap showNuc
                           tell "\t"
                           drip_seq Indel_seq >>= tell . foldMap showNuc
                           pure (Alt2, True, 1)

            IndelAltMulti -> do na <- fromIntegral <$> drip_w8 Typecode
                                drip_seq Indel_seq >>= tell . foldMap showNuc
                                tell "\t"
                                replicateM_ (na-1) $
                                    drip_seq Indel_seq >>= tell . foldMap showNuc >> tell ","
                                drip_seq Indel_seq >>= tell . foldMap showNuc
                                pure (Alt2, True, na)

            SetChrom -> error "What?!"
            SetPos   -> error "What?!"

    tell "\t"
    tell . buildFloat =<< drip_float Qualities
    tell "\t.\t"

    when isindel $ tell "INDEL;"
    cond_int   "IDV="  ";" Idv
    cond_float "IMF="  ";" Imf
    dp <- drip_w32 Dp
    tell $ "DP=" <> int32Dec dp <> ";"
    cond_float_unit "VDB="  ";" Vdb
    cond_float_sunit "SGB="  ";" Sgb
    cond_float_unit "RPB="  ";" Rpb
    cond_float_unit "MQB="  ";" Mqb
    cond_float_unit "MQSB=" ";" Mqsb
    cond_float_unit "BQB="  ";" Bqb
    cond_float      "MQ0F=" ";" Mq0f
    cond_float_unit "AF1="  ";" Af1
    cond_int   "AC1="  ";" Ac1

    let tell_dp4 a b c d = tell $ "DP4=" <> int32Dec a <> "," <> int32Dec b <> "," <> int32Dec c <> "," <> int32Dec d
    dp4a <- fromIntegral <$> drip_i32 Dp4
    dp4b <- fromIntegral <$> drip_i32 Dp4
    dp4c <- fromIntegral <$> drip_i32 Dp4
    dp4d <- fromIntegral <$> drip_i32 Dp4
    case gt of
        Ref2 -> tell_dp4 (dp4a + dp `div` 2) (dp4b + dp `div` 2)  dp4c                dp4d
        Het  -> tell_dp4 (dp4a + dp `div` 4) (dp4b + dp `div` 4) (dp4c + dp `div` 4) (dp4d + dp `div` 4)
        Alt2 -> tell_dp4  dp4a                dp4b               (dp4c + dp `div` 2) (dp4d + dp `div` 2)

    cond_int ";MQ=" "" Mq
    cond_float ";FQ=" "" Fq
    drip_mfloat Pv >>= mapM_ (\a -> do
        b <- drip_float Pv ; c <- drip_float Pv ; d <- drip_float Pv
        tell $ ";PV4=" <> buildFloat a <> "," <> buildFloat b <> "," <> buildFloat c <> "," <> buildFloat d)
    tell "\tGT:PL\t"

    tell $ case gt of Ref2 -> "0/0:"
                      Het  -> "0/1:"
                      Alt2 -> "1/1:"

    let npl = (1 + nalt) * (2 + nalt) `div` 2
    replicateM_ (npl-1) $ do tell . int32Dec =<< drip_w32 Pl ; tell ","
    tell . int32Dec =<< drip_w32 Pl
    tell "\n"
    pure $ if isindel then Loc chrom pos
                      else Loc chrom (succ pos)
  where
    drip f = T.drip (if use_row_major then toEnum 0 else f)

    cond_int   key sep fld = drip_mw32 fld >>= mapM_ (\i -> tell $ byteString key <> int32Dec   i <> byteString sep)
    cond_float key sep fld = drip_mfloat fld >>= mapM_ (\i -> tell $ byteString key <> buildFloat i <> byteString sep)
    cond_float_unit key s f = drip_mfloat_unit f >>= mapM_ (\i -> tell $ byteString key <> buildFloat i <> byteString s)
    cond_float_sunit key s f = drip_mfloat_sunit f >>= mapM_ (\i -> tell $ byteString key <> buildFloat i <> byteString s)
    drip_seq f = drip_w8 f >>= \l -> map N <$> replicateM (fromIntegral l) (drip_w8 f)

    drip_i8 f = drip f >>= \x -> pure (x :: Int8)
    drip_w8 f = drip f >>= \x -> pure (x :: Word8)
    drip_float f | use_half  = fromHalf <$> drip f
                 | otherwise = drip f >>= \x -> pure (x :: Float)

    drip_mfloat f = drip_float f >>= \x -> pure $ if isNaN x then Nothing else Just x

    drip_mfloat_unit f | not use_unit_ranges = drip_mfloat f
    drip_mfloat_unit f = drip_w8 f >>= \x -> pure $ if x == 255 then Nothing else Just (fromIntegral x / 127)

    drip_mfloat_sunit f | not use_unit_ranges = drip_mfloat f
    drip_mfloat_sunit f = drip_w8 f >>= \x -> pure $ if x == 255 then Nothing else Just (fromIntegral x / 127 - 1)

    drip_w32 f = maybe 0 id <$> drip_mw32 f
    drip_mw32 f = drip_w8 f >>= \case
        k | k < 0x80 -> pure $             Just $ fromIntegral k
          | k < 0xC0 -> (\a0 ->            Just $ fromIntegral a0 `shiftL` 0 .|. fromIntegral (k .&. 0x3F) `shiftL` 8)
                        <$> drip_w8 f
          | k < 0xE0 -> (\a8 a0 ->         Just $ fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                                  fromIntegral (k .&. 0x1F) `shiftL` 16)
                        <$> drip_w8 f <*> drip_w8 f
          | k < 0xF0 -> (\a16 a8 a0 ->     Just $ fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                                  fromIntegral a16 `shiftL` 16 .|. fromIntegral (k .&. 0xF) `shiftL` 24)
                        <$> drip_w8 f <*> drip_w8 f <*> drip_w8 f
        0xF0 ->         (\a24 a16 a8 a0 -> Just $ fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                                  fromIntegral a16 `shiftL` 16 .|. fromIntegral a24 `shiftL` 24)
                        <$> drip_w8 f <*> drip_w8 f <*> drip_w8 f <*> drip_w8 f
        0xFF -> pure (Nothing :: Maybe Int32)
        k -> error $ printf "invalid word key %x" k

    -- shift left, then shift again serves to mask the key and extend the sign properly
    drip_i32 f = drip_i8 f >>= \case
        k | k >=    0 -> pure               (fromIntegral (shiftL k 1) `shiftR` 1 :: Int32)
          | k < -0x40 -> (\a0 ->             fromIntegral a0 `shiftL` 0 .|. fromIntegral (shiftL k 2) `shiftL` 6)
                         <$> drip_w8 f
          | k < -0x20 -> (\a8 a0 ->          fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                             fromIntegral (shiftL k 3) `shiftL` 13)
                         <$> drip_w8 f <*> drip_w8 f
          | k < -0x10 -> (\a16 a8 a0 ->      fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                             fromIntegral a16 `shiftL` 16 .|. fromIntegral (shiftL k 4) `shiftL` 20)
                         <$> drip_w8 f <*> drip_w8 f <*> drip_w8 f
          | k <  -0x8 -> (\a24 a16 a8 a0 ->  fromIntegral a0 `shiftL` 0 .|. fromIntegral a8 `shiftL` 8 .|.
                                             fromIntegral a16 `shiftL` 16 .|. fromIntegral a24 `shiftL` 24 .|.
                                             fromIntegral (shiftL k 5) `shiftL` 27)
                         <$> drip_w8 f <*> drip_w8 f <*> drip_w8 f <*> drip_w8 f
          | otherwise -> error $ printf "invalid integer key %x" k

    -- Tries to replicate the output format libhts uses.  It may not be
    -- perfect, but it looks good enough.
    buildFloat :: Float -> Builder
    buildFloat = BP.primBounded $ BP.boudedPrim 20 ftoa
      where
        ftoa 0 p = poke p 48 >> pure (plusPtr p 1)
        ftoa d p = do l <- strfromf p 20 (if use_half then "%.4g"# else "%.6g"#) d
                      if d >= 0.0001 && d <= 999999
                        then walk $ plusPtr p (fromIntegral $ l-1)
                        else pure $ plusPtr p (fromIntegral l)

        -- removes traling zeros and a trailing dot
        walk p = peek p >>= \case
                    48 -> walk (plusPtr p (-1))
                    46 -> pure p
                    _  -> pure (plusPtr p 1)

foreign import ccall unsafe "stdlib.h strtof"   strtof   :: Ptr CChar -> Ptr (Ptr CChar) -> IO Float
foreign import ccall unsafe "stdlib.h strfromf" strfromf :: Ptr Word8 -> CSize -> Addr# -> Float -> IO CInt
