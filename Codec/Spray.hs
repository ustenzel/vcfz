{-# LANGUAGE ScopedTypeVariables #-}
module Codec.Spray where

import BasePrelude
import Codec.Stream                         ( CodedStream(..) )
import Control.Monad.Trans.RWS.Strict       ( RWST(..), ask )
import Data.ByteString.Internal             ( ByteString(PS) )
import Foreign.ForeignPtr.Unsafe            ( unsafeForeignPtrToPtr )
import Foreign.Marshal.Alloc                ( malloc, free )
import Foreign.Marshal.Utils                ( copyBytes )
import Streaming                            ( Of(..), MonadIO(..) )

import qualified Data.ByteString              as B
import qualified Data.ByteString.Streaming    as S
import qualified Data.Vector.Mutable          as V
import qualified Data.Vector.Storable.Mutable as W
import qualified Streaming.Prelude            as Q

data Spray t = Spray { buffer :: V.IOVector (ForeignPtr Word8)
                     , next   :: W.IOVector (Ptr Word8)
                     , end    :: W.IOVector (Ptr Word8)
                     , strm   :: V.IOVector (CodedStream)
                     , queues :: V.IOVector ([B.ByteString],[B.ByteString]) }

-- | Input is a vector of desired buffer sizes.
newSpray :: (MonadIO m, Bounded t, Enum t) => CodedStream -> m (Spray t)
newSpray = newSpray' minBound maxBound
  where
    newSpray' :: (MonadIO m, Enum t) => t -> t -> CodedStream -> m (Spray t)
    newSpray' x y decomp = liftIO $ do
        let l = fromEnum y - fromEnum x + 1
        p0     <- newForeignPtr_ nullPtr
        buffer <- V.replicate l p0
        next   <- W.replicate l nullPtr
        end    <- W.replicate l nullPtr
        strm   <- V.replicate l decomp
        queues <- V.replicate l ([],[])
        return Spray{..}

sprayEnded :: (MonadIO m, Monoid w, Enum i, Bounded i) => i -> RWST (Spray i) w (S.ByteString m r) m Bool
sprayEnded t = do
    Spray{..} <- ask
    let i = fromEnum t
    e0 <- liftIO $ W.read end i
    n0 <- liftIO $ W.read next i
    if n0 /= e0
      then pure False     -- buffer is not yet empty
      else do fillBuf t
              liftIO (V.read strm i) >>= \case
                  StreamEnd -> pure True
                  _         -> pure False


drip :: (MonadIO m, Storable a, Monoid w, Enum i, Bounded i) => i -> RWST (Spray i) w (S.ByteString m r) m a
drip = drip' undefined
  where
    drip' :: (MonadIO m, Storable a, Monoid w, Enum i, Bounded i) => a -> i -> RWST (Spray i) w (S.ByteString m r) m a
    drip' a0 t = do
        let i = fromEnum t
        Spray{..} <- ask
        e0 <- liftIO $ W.read end i
        n0 <- liftIO $ W.read next i

        let sz = sizeOf a0
        if minusPtr e0 n0 < sz then do
            pa <- liftIO malloc
            liftIO $ copyBytes pa (castPtr n0) (minusPtr e0 n0)

            let fill 0 = pure ()
                fill l = do fillBuf t
                            e <- liftIO $ W.read end i
                            n <- liftIO $ W.read next i
                            let ll = min l (minusPtr e n)
                            liftIO $ copyBytes (plusPtr pa (sz - l)) (castPtr n) ll
                            liftIO $ W.write next i $ plusPtr n ll
                            fill (l - ll)

            fill (sz - minusPtr e0 n0)
            a <- liftIO $ peek pa
            liftIO $ free pa
            liftIO $ V.read buffer i >>= touchForeignPtr
            pure a

          else do
            liftIO $ W.write next i $ plusPtr n0 sz
            a <- liftIO (peek (castPtr n0))
            liftIO $ V.read buffer i >>= touchForeignPtr
            pure a


fillBuf :: (MonadIO m, Monoid w, Bounded i, Enum i) => i -> RWST (Spray i) w (S.ByteString m r) m ()
fillBuf t = ask >>= fillBuf' minBound maxBound
  where
    fillBuf' :: (MonadIO m, Monoid w, Bounded i, Enum i) => i -> i -> Spray i -> RWST r w (S.ByteString m x) m ()
    fillBuf' t_min t_max Spray{..} = liftIO (V.read strm i) >>= go
      where
        i     = fromEnum t
        i_min = fromEnum t_min
        i_max = fromEnum t_max

        go (OutputAvailable (PS fp off len) k) = liftIO $ do
            V.write buffer i fp
            W.write next i (unsafeForeignPtrToPtr fp `plusPtr` off)
            W.write end i (unsafeForeignPtrToPtr fp `plusPtr` (off+len))
            V.write strm i k

        go (StreamEnd) = liftIO $ do
            W.write next i nullPtr
            W.write end i nullPtr
            V.write strm i StreamEnd

        go (CodecError    e) = liftIO $ throwIO $ ErrorCall e
        go (CodecGo       k) = liftIO k >>= go
        go (InputRequired k) = go_input k

        go_input k = liftIO (V.read queues i) >>= \case

          (c:cs,q)                       -> liftIO (V.write queues i (cs,q)) >> go (k c)

          ([  ],q) -> case reverse q of
            c:cs                         -> liftIO (V.write queues i (cs, [])) >> go (k c)
            [  ] -> stateT getChunk >>= \case
              Left    _                  -> go (k mempty)
              Right (j,c)
                | j < i_min || j > i_max -> error $ printf "Tag value %d out of range (%d,%d)." j i_min i_max
                | i == j                 -> go (k c)
                | otherwise              -> liftIO (V.modify queues (\(u,v) -> (u,c:v)) j) >> go_input k

stateT :: (Applicative m, Monoid w) => (s -> m (a,s)) -> RWST r w s m a
stateT f = RWST $ \_ s -> (\(a,t) -> (a,t,mempty)) <$> f s

getChunk :: MonadIO m => S.ByteString m x -> m (Either x (Int, B.ByteString), S.ByteString m x)
getChunk s0 = do
    key :> s1 <- Q.toList $ S.unpack $ S.splitAt 3 s0
    case key of
        [       ] -> S.effects s1 >>= \r -> pure (Left r, pure r)
        [i,l0,l8] -> do
            let l = fromIntegral l0 .|. fromIntegral l8 `shiftL` 8
            c :> s2 <- S.toStrict $ S.splitAt l s1
            pure (Right (fromIntegral i,c), s2)
        _ -> error "framing error (EOF?)"

