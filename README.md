# VCFZ

An experiment in compressing VCF.

## Method

VCF is a ridiculously inefficient format; it's both bulky and expensive
to parse.  A few obvious ways to make it smaller:

* Use a better general purpose compressor.  `xz` cuts the file size in
  half compared to `gzip`.  It's very slow, though, and parsing remains
  expensive.  However, we can use `liblzma` (the engine behind `xz`)
  internally.  This is less agonizingly slow, it only requires more
  memory (about 2GB for compression, about 100MB for decompression).

* Use a binary format.  The natural choice would be `bcf`, but in my
  case, `bcftools` failed to parse the input file.  So it's a custom
  binary format or nothing.

* Use an efficient binary encoding.  Most integers are small, and using
  an encoding that uses just one byte for small numbers makes the job of
  a general compressor easier.

* Encode less information about the layout of each record.  Most records
  have the same layout anyway, so there is no point in constantly
  repeating the field names.  For this experiment, the layout is
  hardcoded.

* Use column oriented storage, and compress each column separately.
  This is the main idea of this experiment:  if a column is compressed
  in isolation, it is much more regular than the mess that is VCF, which
  means it compresses much better.  (This point will be the baseline for
  the savings reported below.)

* Many floating point numbers are stored with unneeded precision.
  Storing 16 bit "half floats" instead cuts the file size down by about
  10%.  This is optional, because loses some (unneeded) precision.

* Some floating point numbers have a very restricted range and need even
  less precision.  Those can be stored in a single byte, and doing so
  shaves another 5% off the file size.  This is also optional, because
  of the lost precision.

* The DP4 field can be predicted from DP and GT.  If only the difference
  between prediction and actual values is stored, the values become
  smaller and compress better.  Doing so saves 2.5% of file size and has
  no downsides.

* MQ0F could be transformed into an integer, which is presumably stored
  more efficiently.  While this works, the difference is tiny, so I
  dropped the idea.

## Results

    size time
raw ~200GB -
gzip ~20GB -
xz 11.7GB ~15h
VCFZ/gzip 9.4GB 51h
VCFZ/lzma 6.8GB 61.5h
VCFZ/hacks 5.9GB 59.5h

# Row-major vs. Column-major Layout

Looks like column-major is always smaller than row-major, by 5-20%,
depending on settings and the compression algorithm.  However, if
half-floats are combined with lzma, then row-major layout is smaller by
6.4%.

How can this make sense?  Clearly, there is some redundancy within
records that lzma can capitalize on only if half-floats are used.  Could
it be that certain values always go together, but the excess precision
of 32-bit floats makes them not repeat exactly?

Anyway, the lesson might be that column-major storage isn't worth the
trouble, because just using lzma is good enough and much easier.

## TODO

* Statistics:  which field uses all the space?
* Profiling:  what takes so fucking long?!
* Row-major mode doesn't decode correctly (fields are emitted in the
  wrong order)


